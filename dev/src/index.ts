import Vue from 'vue'
import App from './App.vue'
import Fragment from 'vue-fragment'
import VueCompositionAPI from '@vue/composition-api'

Vue.use(Fragment.Plugin)
Vue.use(VueCompositionAPI)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
