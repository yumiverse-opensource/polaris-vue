import { ContextProvider } from '../ContextProvider';
import type {ThemeConfig} from '../../utilities/theme';
import {ThemeProvider} from '../ThemeProvider';
import {MediaQueryProvider} from '../MediaQueryProvider';
import {FocusManager} from '../FocusManager';
import {I18n} from '../../utilities/i18n/I18n';
import {
  ScrollLockManager,
} from '../../utilities/scroll-lock-manager/scroll-lock-manager';
import {
  StickyManager,
} from '../../utilities/sticky-manager/sticky-manager';
/* import {LinkContext, LinkLikeComponent} from '../../utilities/link'; */
/* import {FeaturesConfig, FeaturesContext} from '../../utilities/features'; */
import {
  UniqueIdFactory,
  globalIdGeneratorFactory,
} from '../../utilities/unique-id/unique-id-factory';

import './AppProvider.scss';

interface State {
  intl: I18n;
  link: LinkLikeComponent | undefined;
}

export interface AppProviderProps {
  /** A locale object or array of locale objects that overrides default translations. If specifying an array then your primary language dictionary should come first, followed by your fallback language dictionaries */
  i18n: ConstructorParameters<typeof I18n>[0];
  /** A custom component to use for all links used by Polaris components */
  linkComponent?: LinkLikeComponent;
  /** Custom logos and colors provided to select components */
  theme?: ThemeConfig;
  /** For toggling features */
  features?: FeaturesConfig;
}

export const AppProvider = {
  props: ['i18n', 'linkComponent', 'theme', 'features'],
  created() {
    this.stickyManager = new StickyManager()
    this.scrollLockManager = new ScrollLockManager(),
    this.uniqueIdFactory = new UniqueIdFactory(globalIdGeneratorFactory)
  },
  data() {
    return {
      link: this.linkComponent,
      intl: new I18n(this.i18n),
    }
  },
  mounted() {
    if (document != null) {
      this.stickyManager.setContainer(document);
    }
  },
  watch: {
    i18n(i18n, prevI18n) {
      if (i18n === prevI18n) {
        return;
      }
      this.intl = new I18n(i18n)
    },
    i18n(linkComponent, prevLinkComponent) {
      if (linkComponent === prevLinkComponent) {
        return;
      }
      this.link = linkComponent
    }
  },
  render() {
    const {theme = {}, intl, link} = this;
    const children = this.$slots.default
    const features = {newDesignLanguage: false, ...this.features};

    return (
      <ContextProvider name="Features" value={features}>
        <ContextProvider name="I18n" value={intl}>
          <ContextProvider name="ScrollLockManager" value={this.scrollLockManager}>
            <ContextProvider name="StickyManager" value={this.stickyManager}>
              <ContextProvider name="UniqueIdFactory" value={this.uniqueIdFactory}>
                <ContextProvider name="Link" value={link}>
                  <ThemeProvider theme={theme}>
                    <MediaQueryProvider>
                      <FocusManager>{children}</FocusManager>
                    </MediaQueryProvider>
                  </ThemeProvider>
                </ContextProvider>
              </ContextProvider>
            </ContextProvider>
          </ContextProvider>
        </ContextProvider>
      </ContextProvider>
    );
  }
}
