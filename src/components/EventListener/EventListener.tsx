interface BaseEventProps {
  event: string;
  capture?: boolean;
  handler(event: Event): void;
}

export interface EventListenerProps extends BaseEventProps {
  passive?: boolean;
}

// see https://github.com/oliviertassinari/react-event-listener/
export const EventListener = {
  props: {
    event: {
      type: String,
      required: true
    },
    capture: Boolean,
    handler: Function,
    passive: Boolean
  },
  computed: {
    changed() {
      const {event, handler, capture} = this;
      return [event, handler, capture]
    }
  },
  watch: {
    changed() {
      this.detachListener();
      this.attachListener();
    }
  },
  render() {
    return null;
  },
  mounted() {
    this.attachListener();
  },

  destroyed() {
    this.detachListener();
  },
  methods: {
    attachListener() {
      const {event, handler, capture, passive} = this;
      console.log({event, handler, capture})
      window.addEventListener(event, handler, {capture, passive});
      this.prevProps = {event, handler, capture}
    },
    detachListener() {
      const {event, handler, capture} = this.prevProps || this;
      window.removeEventListener(event, handler, capture);
    }
  }
}
