import { ContextProvider } from '../ContextProvider';

interface Props {
}

type Context = NonNullable<ContextType<typeof FocusManagerContext>>;

export const FocusManager = {
  data() {
    return {
      trapFocusList: []
    }
  },
  methods: {
    setTrapFocusList(list) {
      this.trapFocusList = list
    },
    add(id) {
      const { trapFocusList: list } = this
      this.setTrapFocusList([...list, id])
    },
    remove(id) {
      let removed = true;
      const { trapFocusList: list } = this
      const clone = [...list];
      const index = clone.indexOf(id);
      if (index === -1) {
        removed = false;
      } else {
        clone.splice(index, 1);
      }
      this.setTrapFocusList(clone);
      return removed;
    }
  },
  render() {
    const {
      add,
      trapFocusList,
      remove } = this
    const value = {
      add,
      trapFocusList,
      remove }
    const children = this.$slots.default

    return (
      <ContextProvider name="FocusManagerContext" value={value}>
      {children}
      </ContextProvider>  
    );
  }
}
