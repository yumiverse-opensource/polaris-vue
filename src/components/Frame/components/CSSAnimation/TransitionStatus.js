export var TransitionStatus;
(function (TransitionStatus) {
    TransitionStatus["Entering"] = "entering";
    TransitionStatus["Entered"] = "entered";
    TransitionStatus["Exiting"] = "exiting";
    TransitionStatus["Exited"] = "exited";
})(TransitionStatus || (TransitionStatus = {}));
