import React from 'react';

import type {HeadingTagName} from '../../types';

import styles from './Heading.scss';

export interface HeadingProps {
  /**
   * The element name to use for the heading
   * @default 'h2'
   */
  element?: HeadingTagName;
  /** The content to display inside the heading */
  children?: React.ReactNode;
}

export const Heading = ({props: {element: Element = 'h2'}, children}) => {
  return <Element class={styles.Heading}>{children}</Element>;
}
