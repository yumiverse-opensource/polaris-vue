import { ref, inject, reactive, computed } from '@vue/composition-api';
import { useToggle } from '../utilities/use-toggle';
import {
    UniqueIdFactoryContext,
} from '../../src/utilities/unique-id'
import { useContext, useRef } from 'react'

export const Hello = (prop, ctx) => {
    let state = ref('Hello World!');
    new Promise((resolve) => setTimeout(resolve, 1000)).then(_ => {
        console.log(state)
        state = "Hello world"
        console.log(state)
    })
    return () => (
        <h1>{state.value}
        </h1>
            
    );
};


export const Hello2 = {
    setup() {
        const state = reactive({
            count: 0,
            double: computed(() => state.count * 2)
        })
        const {value: toggleValue, toggle, setTrue, setFalse} = useToggle(true)
      console.log(useToggle(true))
      console.log(toggle)

        const idFactory = useContext(UniqueIdFactoryContext)
        console.log(idFactory)

        const testUseRef = useRef(2)
        console.log(testUseRef.current)
        testUseRef.current = 4

        function increment() {
            state.count++
        }

        return {
            state,
            increment,
            testUseRef,
            toggleValue, toggle, setTrue, setFalse
        }
    },
    render() {
        console.log(this.toggleValue)
        return (<button vOn:click={this.toggle}>
            Count is: { this.state.count }, double is: { this.state.double }
                toggle is : {String(this.toggleValue)}
                testUseRef: {this.testUseRef}
                </button>)
    }
}

