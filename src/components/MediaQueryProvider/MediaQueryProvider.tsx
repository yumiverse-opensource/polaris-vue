import { ContextProvider } from '../ContextProvider';
import Vue from 'vue';
import debounce from 'lodash/debounce';

import {MediaQueryContext} from '../../utilities/media-query';
import {navigationBarCollapsed} from '../../utilities/breakpoints';
import {EventListener} from '../EventListener';

interface Props {
}

export const MediaQueryProvider = {
  data() {
    return {
      isNavigationCollapsed: navigationBarCollapsed().matches
    }
  },
  methods: {
    setIsNavigationCollapsed(value) {
      this.isNavigationCollapsed = value
    },
    handleResize() {
      return debounce(
        () => {
          const {isNavigationCollapsed} = this
          if (isNavigationCollapsed !== navigationBarCollapsed().matches) {
            this.setIsNavigationCollapsed(!isNavigationCollapsed);
          }
        },
        40,
        {trailing: true, leading: true, maxWait: 40},
      )()
    }
  },
  render() {
    const {isNavigationCollapsed} = this
    const children =this.$slots.default
    
    return <ContextProvider name="MediaQueryContext" value={{isNavigationCollapsed}}>
      <EventListener event="resize" handler={this.handleResize} />
      {children}
    </ContextProvider>
  }
}
