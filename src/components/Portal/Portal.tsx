import {useContext} from 'react'
import { reactive, ref, computed, inject, watch, onMounted, onUpdated, onUnmounted } from '@vue/composition-api';
import {ThemeContext} from '../../utilities/theme';
import {globalIdGeneratorFactory} from '../../utilities/unique-id';
import {portal} from '../shared';

export interface PortalProps {
  idPrefix?: string;
  onPortalCreated?(): void;
}

interface State {
  isMounted: boolean;
}

const getUniqueID = globalIdGeneratorFactory('portal-');

export const Portal = {
  defaultProps: {idPrefix: ''},
  contextType: ThemeContext,
  props: {
    idPrefix: String
  },
  setup(props, {emit, listeners, ...rest}) {
    const context = inject(ThemeContext.symbol)
    const state = reactive({isMounted: false})
    console.log(rest)
    watch(
      () => state.isMounted,
      (isMounted, prevIsMounted) => {
        console.log(isMounted)
        const {onPortalCreated = noop} = listeners
        if (!prevIsMounted && isMounted) {
          onPortalCreated()
        }
      }
    )
    
    const portalId = props.idPrefix !== ''
                   ? `${props.idPrefix}-${getUniqueID()}`
                   : getUniqueID()

    return { state, portalId, context }
  },
  mounted() {
    console.log(this.$slots.default)
    this.portalNode = document.createElement('div')
    this.portalNode.appendChild(this.$refs.portalNode)
    this.portalNode.setAttribute(portal.props[0], this.portalId)

    if (this.context != null) {
      const {cssCustomProperties} = this.context;
      if (cssCustomProperties != null) {
        this.portalNode.setAttribute('style', cssCustomProperties);
      } else {
        this.portalNode.removeAttribute('style');
      }
    }

    document.body.appendChild(this.portalNode)
    this.state.isMounted = true
  },updated() {
    if (this.portalNode && this.context != null) {
      const {cssCustomProperties, textColor} = this.context
      if (cssCustomProperties != null) {
        const style = `${cssCustomProperties};color:${textColor};`;
        this.portalNode.setAttribute('style', style);
      } else {
        this.portalNode.removeAttribute('style');
      }
    }
  },destroyed() {

      if (this.portalNode) {
        document.body.removeChild(this.portalNode)
      }
  },
  render() {
    return <div><div ref="portalNode">{this.$slots.default}</div></div>
  }
}

function noop() {}
