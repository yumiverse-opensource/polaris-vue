import {classNames, variationName} from '../../utilities/css';
import Vue from "vue"

import {Item} from './components';
import styles from './Stack.scss';

type Spacing = 'extraTight' | 'tight' | 'loose' | 'extraLoose' | 'none';

type Alignment = 'leading' | 'trailing' | 'center' | 'fill' | 'baseline';

type Distribution =
  | 'equalSpacing'
  | 'leading'
  | 'trailing'
  | 'center'
  | 'fill'
  | 'fillEvenly';

export interface StackProps {
  /** Wrap stack elements to additional rows as needed on small screens (Defaults to true) */
  wrap?: boolean;
  /** Stack the elements vertically */
  vertical?: boolean;
  /** Adjust spacing between elements */
  spacing?: Spacing;
  /** Adjust vertical alignment of elements */
  alignment?: Alignment;
  /** Adjust horizontal alignment of elements */
  distribution?: Distribution;
}

export const Stack = ({
  props: {
    vertical,
    spacing,
    distribution,
    alignment,
    wrap,
  }, children}) => {
    const className = classNames(
      styles.Stack,
      vertical && styles.vertical,
      spacing && styles[variationName('spacing', spacing)],
      distribution && styles[variationName('distribution', distribution)],
      alignment && styles[variationName('alignment', alignment)],
      wrap === false && styles.noWrap,
    );

    const itemMarkup = children.map((child, index) => {
      const props = {key: index};
      if (child.fnOptions && child.fnOptions._Ctor &&
          Object.values(child.fnOptions._Ctor)[0] === Vue.extend(Item)) {
        return ( child );
      }
      return <Item {...props}>{child}</Item>;
    });

    return <div class={className}>{itemMarkup}</div>;
}

Stack.Item = Item;
