import {classNames} from '../../../../utilities/css';
import styles from '../../Stack.scss';

export interface ItemProps {
  /** Fill the remaining horizontal space in the stack with the item  */
  fill?: boolean;
  /**
   * @default false
   */
}

export const Item = ({props: {fill=false}, children}) => {
  const className = classNames(styles.Item, fill && styles['Item-fill']);

  return <div class={className}>{children}</div>;
}
