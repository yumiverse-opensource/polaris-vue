import { ContextProvider } from '../ContextProvider';
import DefaultThemeColors from '@shopify/polaris-tokens/dist-modern/theme/base.json';

import {
  buildThemeContext,
  buildCustomProperties
} from '../../utilities/theme/utils'
import {
  Tokens
} from '../../utilities/theme/tokens'
import { ThemeConfig } from '../../utilities/theme/tokens';
/* import {useFeatures} from '../../utilities/features'; */

type OriginalColorScheme = Required<ThemeConfig['colorScheme']>;
type Inverse = 'inverse';
type InversableColorScheme = OriginalColorScheme | Inverse;

// TS 3.5+ includes the built-in Omit type which does the same thing. But if we
// use that then we break consumers on older versions of TS. Consider removing
// this when we drop support for consumers using TS <3.5 (in v5?)
type Discard<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

interface ThemeProviderThemeConfig extends Discard<ThemeConfig, 'colorScheme'> {
  colorScheme?: InversableColorScheme;
}

interface ThemeProviderProps {
  /** Custom logos and colors provided to select components */
  theme: ThemeProviderThemeConfig;
}

export const ThemeProvider = {
  props: {
    theme: Object
  },
  inject: {
    parentContext: {
      from: 'ThemeContext',
      default: undefined
    },
  },
  render(h) {
    /* const {newDesignLanguage} = useFeatures(); */
    const {newDesignLanguage} = {newDesignLanguage: false}
    const { theme: themeConfig = {}, parentContext } = this
    const children = this.$slots.default
    const isParentThemeProvider = parentContext === undefined;
    const parentColorScheme =
      parentContext && parentContext.colorScheme && parentContext.colorScheme;
    const parentColors =
      parentContext && parentContext.colors && parentContext.colors;

    const {colors, colorScheme, ...rest} = themeConfig;

    const processedThemeConfig = {
      ...rest,
      ...{colorScheme: getColorScheme(colorScheme, parentColorScheme)},
      colors: {
        ...(isParentThemeProvider && DefaultThemeColors),
        ...(parentColors != null && parentColors),
        ...colors,
      },
    };

    const customProperties = buildCustomProperties(processedThemeConfig, newDesignLanguage, Tokens)

    const theme = buildThemeContext(
          processedThemeConfig,
          newDesignLanguage ? customProperties : undefined,
        )

    // We want these values to be empty string instead of `undefined` when not set.
    // Otherwise, setting a style property to `undefined` does not remove it from the DOM.
    const backgroundColor = customProperties['--p-background'] || '';
    const color = customProperties['--p-text'] || '';

    if (isParentThemeProvider) {
      document.body.style.backgroundColor = backgroundColor;
      document.body.style.color = color;
    }

    const style = {...customProperties, ...(!isParentThemeProvider && {color})};

    return (
      <ContextProvider name="ThemeContext" value={{...theme, textColor: color}}
        style={style}>
        {children}
      </ContextProvider>
    );
  }
}

function isInverseColorScheme(
  colorScheme?: InversableColorScheme,
): colorScheme is Inverse {
  return colorScheme === 'inverse';
}

function getColorScheme(
  colorScheme: InversableColorScheme | undefined,
  parentColorScheme: OriginalColorScheme | undefined,
) {
  if (colorScheme == null) {
    return parentColorScheme || 'light';
  } else if (isInverseColorScheme(colorScheme)) {
    return parentColorScheme === 'dark' || parentColorScheme === undefined
      ? 'light'
      : 'dark';
  } else {
    return colorScheme;
  }
}
