import styles from './VisuallyHidden.scss';

export const VisuallyHidden = (h, {children})=> {
  return <span class={styles.VisuallyHidden}>{children}</span>;
}
