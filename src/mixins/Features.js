export default {
  computed: {
    newDesignLanguage: function() {
      return this.FeaturesContext.newDesignLanguage
    },
  },
  inject: {
    FeaturesContext: { default: {newDesignLanguage: false} }
  }
}
