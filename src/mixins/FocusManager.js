import { MissingAppProviderError } from '../utilities/errors'
import UniqueId from './UniqueId'

export default {
  mixins: [UniqueId],
  inject: {
    focusManager: {
      from: 'FocusManagerContext',
      default: undefined
    }
  },
  data() {
    return {
      id: this.useUniqueId()
    }
  },
  computed: {
    canSafelyFocus() {
      const {id} = this.id
      const { trapFocusList } = this.focusManager
      return trapFocusList[0] === id
    }
  },
  created() {
    if (!this.focusManager) {
      throw new MissingAppProviderError('No FocusManager was provided.');
    }
  },
  mounted() {
    this.focusManager.add(this.id)
  },
  destroyed() {
    this.focusManager.remove(this.id)
  }
}
