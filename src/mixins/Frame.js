export default {
  inject: {
    FrameContext: {default: undefined},
  },
  created() {
    const frame = this.FrameContext

    if (!frame) {
      throw new Error(
      'No Frame context was provided. Your component must be wrapped in a <Frame> component. See https://polaris.shopify.com/components/structure/frame for implementation instructions.',
      )
    }
  }
}
