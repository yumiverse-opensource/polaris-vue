import { MissingAppProviderError } from '../utilities/errors'

export default {
  inject: {
    I18nContext: {default: undefined},
  },
  computed: {
    i18n() {
      return this.I18nContext
    }
  },
  created() {
    if (!this.i18n) {
      throw new MissingAppProviderError('No i18n was provided.');
    }
  }
}
