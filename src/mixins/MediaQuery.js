import { MissingAppProviderError } from '../utilities/errors'

export default {
  inject: {
    mediaQuery: {
      from: 'MediaQueryContext',
      default: undefined
    }
  },
  computed: {
    isNavigationCollapsed() {
      const {isNavigationCollapsed} = this.mediaQuery
      return isNavigationCollapsed
    }
  },
  created() {
    if (!this.mediaQuery) {
      throw new Error('No mediaQuery was provided. Your application must be wrapped in an <AppProvider> component. See https://polaris.shopify.com/components/structure/app-provider for implementation instructions.');
    }
  }
}
