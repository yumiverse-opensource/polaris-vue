import { MissingAppProviderError } from '../utilities/errors'

export default {
  inject: {
    scrollLockManager: {
      from: "ScrollLockManagerContext",
      default: undefined
    }
  },
  created() {
    if (!this.scrollLockManager) {
      throw new MissingAppProviderError('No ScrollLockManager was provided.');
    }
  }
}
