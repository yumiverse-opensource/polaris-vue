import { MissingAppProviderError } from '../utilities/errors'

export default {
  inject: {
    ThemeContext: {
      default: undefined
    }
  },
  created() {
    if (!this.ThemeContext) {
      throw new MissingAppProviderError('No Theme was provided.');
    }
  }
}
