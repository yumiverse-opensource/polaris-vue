import { MissingAppProviderError } from '../utilities/errors'

export default {
  // By using a ref to store the uniqueId for each invocation of the hook and
  // checking that it is not already populated we ensure that we don’t generate
  // a new ID on every re-render of a component.
  uniqueIdRef: null,
  inject: {
    idFactory: {
      from: 'UniqueIdFactoryContext',
      default: undefined
    }
  },
  methods: {
    useUniqueId(prefix = '', overrideId = '') {
      if (!this.idFactory) {
        throw new MissingAppProviderError('No UniqueIdFactory was provided.');
      }
      // If an override was specified, then use that instead of using a unique ID
      // Hooks can’t be called conditionally so this has to go after all use* calls
      if (overrideId) {
        return overrideId;
      }
      // If a unique id has not yet been generated, then get a new one
      if (!this.uniqueIdRef) {
        this.uniqueIdRef = this.idFactory.nextId(prefix)
      }
      return this.uniqueIdRef;
    }
  }
}
