import {
  provide,
  isRef,
  ref,
  reactive,
  watchEffect,
  watch,
  inject,
  isReactive,
  /* readonly, */
  toRefs
} from '@vue/composition-api'

const valuePropOption = (value) => {
  let options = {
    ...value && {default: value}
  }
  const type = typeof value
  switch (type) {
    case 'undefined':
      break;
    case 'string':
      options.type = String
      break;
    case 'number':
      options.type = Number
      break;
    case 'boolean':
      options.type = Boolean
      break;
    case 'symbol':
      options.type = Symbol
      break;
    case 'function':
      options.type = Function
      break;
    case 'array':
      options.type = Array
      break;
    case 'object':
      if (Object.prototype.toString.call(value) === '[object Date]') {
        options.type = Date
      } else {
        options.type = Object
      }
      break;
    default:
      break;
  }
  return options
}

export function createContext<T>(
  defaultValue: T,
  /* calculateChangedBits: ?(a: T, b: T) => number, */
) {
  const symbol = Symbol()
  return {
    symbol,
    Provider: {
      props: {
        value: valuePropOption(defaultValue)
      },
      setup(props) {
        const {value: initial} = props
        const state = initial !== Object(initial) ? ref(initial) : reactive(initial)
        watchEffect(() => {
          if (isRef(state)) {
            state.value = props.value
          } else {
            Object.assign(state, props.value)
          }
        })

        // Using `toRefs()` makes it possible to use
        // spreading in the consuming component.
        // Making the return value `readonly()` prevents
        // users from mutating global state.
        /* provide(symbol, toRefs(readonly(state))) */
        provide(symbol, isReactive(state) ? toRefs(state) : state)

        /* const update = (property, value) => {
         *   state[property] = value;
         * };
         * provide(UserSettingsUpdateSymbol, update); */
      },
      render() {
        // Our provider component is a renderless component
        // it does not render any markup of its own.
        return   <fragment>{this.$slots.default}</fragment>
      }
    },
    Consumer: {
      setup() {
        const value = inject(symbol)
        return {value}
      },
      render() {
        // Our provider component is a renderless component
        // it does not render any markup of its own.
        return   <fragment>{this.$scopedSlots.default(this.value)}</fragment>
      }
    }
  }
}
