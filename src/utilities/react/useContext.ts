import {
  inject
} from '@vue/composition-api'

export default (Context) => {
    return inject(Context.symbol)
}
