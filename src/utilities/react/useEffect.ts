import { onMounted, onUpdated, watch } from '@vue/composition-api'

export default (fn, dependencies=[]) => {
    if (Array.isArray(dependencies)) {
        fn()
        watch(dependencies, fn)
    } else {
        onMounted(fn)
        onUpdated(fn)
    }
    return fn
}
