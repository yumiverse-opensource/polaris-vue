import {
  ref
} from '@vue/composition-api'

export default (initialValue) => {
    const target = ref(initialValue)

    const handler = {
        get: function (target, prop, receiver) {
            if (prop === "current") {
                return target.value
            }
            return Reflect.get(...arguments)
        },
        set: function (target, prop, receiver) {
            if (prop === "current") {
                return target['value'] = receiver
            }
            return Reflect.set(...arguments)
        },
    }

    return new Proxy(target, handler)
}
