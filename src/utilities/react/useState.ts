import {
    isRef,
    ref,
    reactive
} from '@vue/composition-api'

export default (initial) => {
    const state = initial !== Object(initial) ? ref(initial) : reactive(initial)

    function setState(value) {
        console.log(value)
        if (isRef(state)) {
            state.value = typeof value === "function" ? value(state.value) : value
        } else {
            Object.assign(state, typeof value === "function" ? value(state) : value)
        }
    }
    
    return [state, setState]
}
