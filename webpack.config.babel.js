import path from 'path'
import fs from 'fs'
import HtmlPlugin from 'html-webpack-plugin'
import { CleanWebpackPlugin as CleanPlugin } from 'clean-webpack-plugin'
import { VueLoaderPlugin } from 'vue-loader'
import CopyPlugin from 'copy-webpack-plugin'
import {generateScopedName} from './config/rollup/namespaced-classname'
import postcssModules from 'postcss-modules'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import postcssShopify from '@shopify/postcss-plugin'

export default ({production, NODE_ENV, ...environment} = {
  production: false, NODE_ENV: 'development' }) => {
  production = production || NODE_ENV === 'production' 
  return {
    mode: production ? 'production' : 'development',
    entry: {
      app: './dev/src/index.ts'
    },
    ...production ? {} : {
      devtool: 'inline-source-map',
      devServer: {
        contentBase: './dev/dist'
      }
    },
    resolve: {
      extensions: ['.ts', '.js', '.jsx', '.tsx', '.vue', '.json'],
      mainFields: ['module', 'main'],
      alias: {
        vue$: 'vue/dist/vue.esm.js',
        react$: path.resolve(__dirname, '.', 'src/utilities/react/index')
      }
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          use: [
            {
              loader: 'vue-loader',
              options: {
                compilerOptions: {
                  preserveWhitespace: false
                }
              }
            }
          ]
        },
        {
          // test: /\.m?(js|jsx)$/,
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              query: {
                sourceMap: false,
                importLoaders: 1,
                modules: {
                  localIdentName: '[name]-[local]_[hash:base64:5]',
                  auto(resourcePath) {
                    console.log(resourcePath)
                    return true
                  }
                },
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => postcssShopify(),
                sourceMap: false,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: false,
              },
            },
          ],
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [
            'file-loader',
          ],
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/,
          use: [
            'file-loader',
          ],
        }
      ],
    },
    plugins: [
      new CopyPlugin({
        patterns: [
          { from: path.resolve(__dirname, 'dev', 'public'), to: path.resolve(__dirname, 'dev', 'dist')}
        ], options: {
          concurrency: 100 }}),
      new MiniCssExtractPlugin({
        filename: 'styles/[name].[hash:8].css',
        chunkFilename: 'styles/chunks/[name].[hash:8].css'
      }),
      new CleanPlugin({ cleanStaleWebpackAssets: false }),
      new VueLoaderPlugin(),
      new HtmlPlugin({
        title: 'v-Polaris',
        filename: 'index.html',
        template: './dev/src/index.html',
        inject: 'body',
        scriptLoading: 'defer',
        minify: false,
        hash: true,
        cache: true,
        showErrors: true
      }),
    ],
    output: {
      filename: '[name].[contenthash].js',
      path: path.resolve(__dirname, 'dev', 'dist')
    },
    performance: {
      hints: production ? "warning" : false
    },
    optimization: {
      moduleIds: 'hashed',
      usedExports: true,
      runtimeChunk: 'single',
      splitChunks: {
        chunks: 'all',
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'all',
          },
        },
      }
    }
  }
}
